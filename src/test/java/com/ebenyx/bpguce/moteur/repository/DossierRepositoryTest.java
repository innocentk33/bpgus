package com.ebenyx.bpguce.moteur.repository;

import com.ebenyx.bpguce.moteur.entity.Dossier;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class DossierRepositoryTest {

	@Autowired
	private DossierRepository dossierRepository;

	Dossier dossier1;
	Dossier dossier2;

	@Before
	public void before(){
		dossierRepository.deleteAll();

		dossier1 = new Dossier();
		assertNotNull(dossier1);
		assertNull(dossier1.getId());
		//assertNotNull(dossier1.get..);

		dossier2 = new Dossier();
		assertNotNull(dossier2);
		assertNull(dossier2.getId());
		//assertNotNull(dossier2.get..);
	}

	@Test
	public void test(){
		// Test save
		dossier1 = dossierRepository.save(dossier1);
		assertNotNull(dossier1);
		assertNotNull(dossier1.getId());
		assertNotNull(dossier1.getCreateDate());
		//assertEquals("", dossier1.get...);

		dossier2 = dossierRepository.save(dossier2);
		assertNotNull(dossier2);
		assertNotNull(dossier2.getId());
		assertNotNull(dossier2.getCreateDate());
		//assertEquals("", dossier2.get...);

		// Test find by id
		dossierRepository.findById(dossier1.getId()).ifPresent(dossier -> {
			assertNotNull(dossier);
			assertNotNull(dossier.getId());
			//assertEquals("", dossier.get...);
		});

		dossierRepository.findById(dossier2.getId()).ifPresent(dossier -> {
			assertNotNull(dossier);
			assertNotNull(dossier.getId());
			//assertEquals("", dossier.get...);
		});

		// Test find by id
		dossier1 = dossierRepository.findById(dossier1.getId()).get();
		assertNotNull(dossier1);
		assertNotNull(dossier1.getId());
		//assertEquals("", dossier1.get...);

		// Test update
		dossierRepository.findById(dossier2.getId()).ifPresent(dossier -> {
			//dossier2.set...;
			dossier2 = dossierRepository.save(dossier2);

			assertNotNull(dossier2);
			assertNotNull(dossier2.getId());
			assertNotNull(dossier2.getUpdateDate());
			// assertEquals("", dossier2.get...());
		});

		// Test find all
		List <Dossier> dossiers = dossierRepository.findAll();
		assertNotNull(dossiers);
		assertEquals(2, dossiers.size());

		// Test delete
		dossierRepository.delete(dossier1);
		long total = dossierRepository.count();
		assertEquals(1, total);
	}

	@After
	public void destroy() {
		dossierRepository.deleteAll();
	}
}
