package com.ebenyx.bpguce.moteur.repository;

import com.ebenyx.bpguce.moteur.entity.Domiciliation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class DomiciliationRepositoryTest {

	@Autowired
	private DomiciliationRepository domiciliationRepository;

	Domiciliation domiciliation1;
	Domiciliation domiciliation2;

	@Before
	public void before(){
		domiciliationRepository.deleteAll();

		domiciliation1 = new Domiciliation();
		assertNotNull(domiciliation1);
		assertNull(domiciliation1.getId());
		//assertNotNull(domiciliation1.get..);

		domiciliation2 = new Domiciliation();
		assertNotNull(domiciliation2);
		assertNull(domiciliation2.getId());
		//assertNotNull(domiciliation2.get..);
	}

	@Test
	public void test(){
		// Test save
		domiciliation1 = domiciliationRepository.save(domiciliation1);
		assertNotNull(domiciliation1);
		assertNotNull(domiciliation1.getId());
		assertNotNull(domiciliation1.getCreateDate());
		//assertEquals("", domiciliation1.get...);

		domiciliation2 = domiciliationRepository.save(domiciliation2);
		assertNotNull(domiciliation2);
		assertNotNull(domiciliation2.getId());
		assertNotNull(domiciliation2.getCreateDate());
		//assertEquals("", domiciliation2.get...);

		// Test find by id
		domiciliationRepository.findById(domiciliation1.getId()).ifPresent(domiciliation -> {
			assertNotNull(domiciliation);
			assertNotNull(domiciliation.getId());
			//assertEquals("", domiciliation.get...);
		});

		domiciliationRepository.findById(domiciliation2.getId()).ifPresent(domiciliation -> {
			assertNotNull(domiciliation);
			assertNotNull(domiciliation.getId());
			//assertEquals("", domiciliation.get...);
		});

		// Test find by id
		domiciliation1 = domiciliationRepository.findById(domiciliation1.getId()).get();
		assertNotNull(domiciliation1);
		assertNotNull(domiciliation1.getId());
		//assertEquals("", domiciliation1.get...);

		// Test update
		domiciliationRepository.findById(domiciliation2.getId()).ifPresent(domiciliation -> {
			//domiciliation2.set...;
			domiciliation2 = domiciliationRepository.save(domiciliation2);

			assertNotNull(domiciliation2);
			assertNotNull(domiciliation2.getId());
			assertNotNull(domiciliation2.getUpdateDate());
			// assertEquals("", domiciliation2.get...());
		});

		// Test find all
		List <Domiciliation> domiciliations = domiciliationRepository.findAll();
		assertNotNull(domiciliations);
		assertEquals(2, domiciliations.size());

		// Test delete
		domiciliationRepository.delete(domiciliation1);
		long total = domiciliationRepository.count();
		assertEquals(1, total);
	}

	@After
	public void destroy() {
		domiciliationRepository.deleteAll();
	}
}
