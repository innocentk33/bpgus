package com.ebenyx.bpguce.moteur.repository;

import com.ebenyx.bpguce.moteur.entity.Fichier;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class FichierRepositoryTest {

	@Autowired
	private FichierRepository fichierRepository;

	Fichier fichier1;
	Fichier fichier2;

	@Before
	public void before(){
		fichierRepository.deleteAll();

		fichier1 = new Fichier();
		assertNotNull(fichier1);
		assertNull(fichier1.getId());
		//assertNotNull(fichier1.get..);

		fichier2 = new Fichier();
		assertNotNull(fichier2);
		assertNull(fichier2.getId());
		//assertNotNull(fichier2.get..);
	}

	@Test
	public void test(){
		// Test save
		fichier1 = fichierRepository.save(fichier1);
		assertNotNull(fichier1);
		assertNotNull(fichier1.getId());
		assertNotNull(fichier1.getCreateDate());
		//assertEquals("", fichier1.get...);

		fichier2 = fichierRepository.save(fichier2);
		assertNotNull(fichier2);
		assertNotNull(fichier2.getId());
		assertNotNull(fichier2.getCreateDate());
		//assertEquals("", fichier2.get...);

		// Test find by id
		fichierRepository.findById(fichier1.getId()).ifPresent(fichier -> {
			assertNotNull(fichier);
			assertNotNull(fichier.getId());
			//assertEquals("", fichier.get...);
		});

		fichierRepository.findById(fichier2.getId()).ifPresent(fichier -> {
			assertNotNull(fichier);
			assertNotNull(fichier.getId());
			//assertEquals("", fichier.get...);
		});

		// Test find by id
		fichier1 = fichierRepository.findById(fichier1.getId()).get();
		assertNotNull(fichier1);
		assertNotNull(fichier1.getId());
		//assertEquals("", fichier1.get...);

		// Test update
		fichierRepository.findById(fichier2.getId()).ifPresent(fichier -> {
			//fichier2.set...;
			fichier2 = fichierRepository.save(fichier2);

			assertNotNull(fichier2);
			assertNotNull(fichier2.getId());
			assertNotNull(fichier2.getUpdateDate());
			// assertEquals("", fichier2.get...());
		});

		// Test find all
		List <Fichier> fichiers = fichierRepository.findAll();
		assertNotNull(fichiers);
		assertEquals(2, fichiers.size());

		// Test delete
		fichierRepository.delete(fichier1);
		long total = fichierRepository.count();
		assertEquals(1, total);
	}

	@After
	public void destroy() {
		fichierRepository.deleteAll();
	}
}
