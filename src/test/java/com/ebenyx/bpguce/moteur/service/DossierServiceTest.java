package com.ebenyx.bpguce.moteur.service;

import com.ebenyx.bpguce.moteur.entity.Dossier;
import com.ebenyx.bpguce.moteur.json.request.DossierJsonRequest;
import com.ebenyx.bpguce.utils.Constants;
import com.ebenyx.bpguce.utils.error.ErrorResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class DossierServiceTest {

	@Autowired
	private DossierService dossierService;

	DossierJsonRequest dossierJsonRequest;

	@Before
	public void setUp() {
		dossierJsonRequest = new DossierJsonRequest();
	}

	@Test
	public void test(){
		// Test before save
		ErrorResponse <Dossier> errorResponse = dossierService.beforeSave(dossierJsonRequest);
		assertNotNull(errorResponse);
		assertEquals(errorResponse.getError(), false);

		// Test save
		Dossier dossier = errorResponse.getEntity();
		dossier = dossierService.save(dossier);
		assertNotNull(dossier);
		assertNotNull(dossier.getId());
		assertNotNull(dossier.getCreateDate());
		//assertEquals("", dossier.get...);

		// Test find one
		dossier = dossierService.findOne(dossier.getId());
		assertNotNull(dossier);
		assertNotNull(dossier.getId());
		//assertEquals("", dossier.get...);

		// Test page all
		Page <Dossier> dossiers = dossierService.load(PageRequest.of(0, Constants.DEFAULT_PAGE_SIZE, Constants.DEFAULT_SORT_DIRECTION, "createDate"), null);
		assertNotNull(dossiers);
		assertNotNull(dossiers.getContent());
		assertEquals(1, dossiers.getTotalElements());

		// Test before delete
		errorResponse = dossierService.beforeDelete(dossier);
		assertNotNull(errorResponse);
		assertEquals(errorResponse.getError(), false);

		// Test delete
		dossierService.delete(dossier);
	}

	@After
	public void destroy() {

	}
}
