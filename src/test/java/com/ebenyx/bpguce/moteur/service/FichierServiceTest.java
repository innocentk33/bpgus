package com.ebenyx.bpguce.moteur.service;

import com.ebenyx.bpguce.moteur.entity.Fichier;
import com.ebenyx.bpguce.moteur.json.request.FichierJsonRequest;
import com.ebenyx.bpguce.utils.Constants;
import com.ebenyx.bpguce.utils.error.ErrorResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class FichierServiceTest {

	@Autowired
	private FichierService fichierService;

	FichierJsonRequest fichierJsonRequest;

	@Before
	public void setUp() {
		fichierJsonRequest = new FichierJsonRequest();
	}

	@Test
	public void test(){
		// Test before save
		ErrorResponse <Fichier> errorResponse = fichierService.beforeSave(fichierJsonRequest);
		assertNotNull(errorResponse);
		assertEquals(errorResponse.getError(), false);

		// Test save
		Fichier fichier = errorResponse.getEntity();
		fichier = fichierService.save(fichier);
		assertNotNull(fichier);
		assertNotNull(fichier.getId());
		assertNotNull(fichier.getCreateDate());
		//assertEquals("", fichier.get...);

		// Test find one
		fichier = fichierService.findOne(fichier.getId());
		assertNotNull(fichier);
		assertNotNull(fichier.getId());
		//assertEquals("", fichier.get...);

		// Test page all
		Page <Fichier> fichiers = fichierService.load(PageRequest.of(0, Constants.DEFAULT_PAGE_SIZE, Constants.DEFAULT_SORT_DIRECTION, "createDate"), null);
		assertNotNull(fichiers);
		assertNotNull(fichiers.getContent());
		assertEquals(1, fichiers.getTotalElements());

		// Test before delete
		errorResponse = fichierService.beforeDelete(fichier);
		assertNotNull(errorResponse);
		assertEquals(errorResponse.getError(), false);

		// Test delete
		fichierService.delete(fichier);
	}

	@After
	public void destroy() {

	}
}
