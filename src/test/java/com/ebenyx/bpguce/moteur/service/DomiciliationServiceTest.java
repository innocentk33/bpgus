package com.ebenyx.bpguce.moteur.service;

import com.ebenyx.bpguce.moteur.entity.Domiciliation;
import com.ebenyx.bpguce.moteur.json.request.DomiciliationJsonRequest;
import com.ebenyx.bpguce.utils.Constants;
import com.ebenyx.bpguce.utils.error.ErrorResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.properties")
public class DomiciliationServiceTest {

	@Autowired
	private DomiciliationService domiciliationService;

	DomiciliationJsonRequest domiciliationJsonRequest;

	@Before
	public void setUp() {
		domiciliationJsonRequest = new DomiciliationJsonRequest();
	}

	@Test
	public void test(){
		// Test before save
		ErrorResponse <Domiciliation> errorResponse = domiciliationService.beforeSave(domiciliationJsonRequest);
		assertNotNull(errorResponse);
		assertEquals(errorResponse.getError(), false);

		// Test save
		Domiciliation domiciliation = errorResponse.getEntity();
		domiciliation = domiciliationService.save(domiciliation);
		assertNotNull(domiciliation);
		assertNotNull(domiciliation.getId());
		assertNotNull(domiciliation.getCreateDate());
		//assertEquals("", domiciliation.get...);

		// Test find one
		domiciliation = domiciliationService.findOne(domiciliation.getId());
		assertNotNull(domiciliation);
		assertNotNull(domiciliation.getId());
		//assertEquals("", domiciliation.get...);

		// Test page all
		Page <Domiciliation> domiciliations = domiciliationService.load(PageRequest.of(0, Constants.DEFAULT_PAGE_SIZE, Constants.DEFAULT_SORT_DIRECTION, "createDate"), null);
		assertNotNull(domiciliations);
		assertNotNull(domiciliations.getContent());
		assertEquals(1, domiciliations.getTotalElements());

		// Test before delete
		errorResponse = domiciliationService.beforeDelete(domiciliation);
		assertNotNull(errorResponse);
		assertEquals(errorResponse.getError(), false);

		// Test delete
		domiciliationService.delete(domiciliation);
	}

	@After
	public void destroy() {

	}
}
