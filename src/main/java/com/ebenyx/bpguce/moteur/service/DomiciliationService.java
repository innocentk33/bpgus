package com.ebenyx.bpguce.moteur.service;

import com.ebenyx.bpguce.utils.error.ErrorResponse;
import com.ebenyx.bpguce.moteur.entity.Domiciliation;
import com.ebenyx.bpguce.moteur.json.request.DomiciliationJsonRequest;

import org.springframework.data.domain.Page;;
import org.springframework.data.domain.Pageable;;

public interface DomiciliationService {

	ErrorResponse<Domiciliation> beforeSave(DomiciliationJsonRequest domiciliationJsonRequest);

	Domiciliation save(Domiciliation domiciliation);

	ErrorResponse<Domiciliation> beforeDelete(Domiciliation domiciliation);

	void delete(Domiciliation domiciliation);

	Domiciliation findOne(Long id);

	Page<Domiciliation> load(Pageable pageable, String keyword);

}