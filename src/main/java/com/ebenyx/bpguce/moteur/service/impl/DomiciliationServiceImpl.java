package com.ebenyx.bpguce.moteur.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ebenyx.bpguce.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ebenyx.bpguce.moteur.entity.Domiciliation;
import com.ebenyx.bpguce.moteur.json.request.DomiciliationJsonRequest;
import com.ebenyx.bpguce.moteur.repository.DomiciliationRepository;
import com.ebenyx.bpguce.moteur.service.DomiciliationService;

import com.ebenyx.bpguce.utils.error.ErrorMessage;

import com.ebenyx.bpguce.utils.error.ErrorPattern;

import com.ebenyx.bpguce.utils.error.ErrorResponse;

/**
 * The DomiciliationService class ...
 * @see DomiciliationService
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
@Transactional
@Service("domiciliationService")
public class DomiciliationServiceImpl implements DomiciliationService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final DomiciliationRepository domiciliationRepository;

	@Autowired
	public DomiciliationServiceImpl(DomiciliationRepository domiciliationRepository) {
		this.domiciliationRepository = domiciliationRepository;
	}

	ErrorPattern<Domiciliation> ep;

	/**
	 * <p>This method check and validate domiciliation properties before saving domiciliation</p>
	 * @param domiciliationJsonRequest contains domiciliation parameters to check and validate
	 * @see ErrorResponse
	 * @see Domiciliation
	 * @see DomiciliationJsonRequest
	 * @return the checking and validation response
	 * @since 1.0
	 */

	@Override
	public ErrorResponse <Domiciliation> beforeSave(DomiciliationJsonRequest domiciliationJsonRequest){
		List <ErrorMessage> errors = new ArrayList <>();
		ep = new ErrorPattern <>();

		Domiciliation domiciliation = new Domiciliation();

		if (domiciliationJsonRequest == null){
			logger.warn("Domiciliation save error : domiciliation null");
			errors = ep.getError(0, "domiciliation can not be null", "object domiciliation can not be null", errors);
		} else {

		}
		return ep.errorResultBuilder(errors, domiciliation);
	}

	/**
	 * <p>This method persist domiciliation properties in database</p>
	 * @param domiciliation contains domiciliation parameters to check and validate
	 * @return the domiciliation persist in database
	 * @see Domiciliation
	 * @since 1.0
	 */

	@Override
	public Domiciliation save(Domiciliation domiciliation) {
		if (domiciliation == null){
			logger.warn("Domiciliation save failed");
			throw new ValidationException("domiciliation can not be null");
		}

		domiciliation = domiciliationRepository.save(domiciliation);
		logger.info("Domiciliation save successfully", domiciliation);
		return domiciliation;
	}

	/**
	 * <p>This method check domiciliation before deleting</p>
	 * @param domiciliation to check
	 * @see Domiciliation
	 * @return the checking response
	 * @see ErrorResponse
	 * @since 1.0
	 */

	@Override
	public ErrorResponse<Domiciliation> beforeDelete(Domiciliation domiciliation){
		List<ErrorMessage> errors = new ArrayList<>();
		ep = new ErrorPattern<>();
		if(domiciliation == null){
			errors = ep.getError(0, "Domiciliation not exist", "Domiciliation not exist", errors);
		}
		return ep.errorResultBuilder(errors, domiciliation);
	}

	/**
	 * <p>This method delete domiciliation in database</p>
	 * @param domiciliation to delete
	 * @see Domiciliation
	 * @since 1.0
	 */

	@Override
	public void delete(Domiciliation domiciliation) {
		if (domiciliation == null){
			logger.warn("Domiciliation delete failed");
			throw new ValidationException("domiciliation can not be null");
		}

		domiciliationRepository.delete(domiciliation);
		logger.info("Domiciliation delete successfully", domiciliation);
	}

	/**
	 * <p>This method find domiciliation by id in database</p>
	 * @param id the domiciliation id in database
	 * @return the domiciliation found in database
	 * @see Domiciliation
	 * @since 1.0
	 */

	@Override
	public Domiciliation findOne(Long id) {
		if (id == null){
			logger.warn("Domiciliation find by id failed");
			throw new ValidationException("id can not be null");
		}

		Optional<Domiciliation> domiciliation = domiciliationRepository.findById(id);
		if(domiciliation.isPresent()){
			logger.info("Domiciliation find by id successfully", domiciliation);
			return domiciliation.get();
		} else {
			logger.info("Domiciliation find by id not exist", domiciliation);
			return null;
		}
	}

	/**
	 * <p>This method page domiciliations in database</p>
	 * @return domiciliations page found in database
	 * @see Domiciliation
	 * @since 1.0
	 */

	@Override
		public Page <Domiciliation> load(Pageable pageable, String keyword) {
		Page<Domiciliation> pages;
		if(keyword == null || keyword.equals(""))
			pages = domiciliationRepository.findAll(pageable);
		else
			pages = null;
		logger.info("All domiciliations found successfully", pages.getTotalPages());
		return pages;
	}
}
