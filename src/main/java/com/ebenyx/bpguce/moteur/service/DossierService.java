package com.ebenyx.bpguce.moteur.service;

import com.ebenyx.bpguce.utils.error.ErrorResponse;
import com.ebenyx.bpguce.moteur.entity.Dossier;
import com.ebenyx.bpguce.moteur.json.request.DossierJsonRequest;

import org.springframework.data.domain.Page;;
import org.springframework.data.domain.Pageable;;

public interface DossierService {

	ErrorResponse<Dossier> beforeSave(DossierJsonRequest dossierJsonRequest);

	Dossier save(Dossier dossier);

	ErrorResponse<Dossier> beforeDelete(Dossier dossier);

	void delete(Dossier dossier);

	Dossier findOne(Long id);

	Page<Dossier> load(Pageable pageable, String keyword);

}