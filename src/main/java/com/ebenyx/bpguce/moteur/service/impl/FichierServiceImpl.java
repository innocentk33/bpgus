package com.ebenyx.bpguce.moteur.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ebenyx.bpguce.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ebenyx.bpguce.moteur.entity.Fichier;
import com.ebenyx.bpguce.moteur.json.request.FichierJsonRequest;
import com.ebenyx.bpguce.moteur.repository.FichierRepository;
import com.ebenyx.bpguce.moteur.service.FichierService;

import com.ebenyx.bpguce.utils.error.ErrorMessage;

import com.ebenyx.bpguce.utils.error.ErrorPattern;

import com.ebenyx.bpguce.utils.error.ErrorResponse;

/**
 * The FichierService class ...
 * @see FichierService
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
@Transactional
@Service("fichierService")
public class FichierServiceImpl implements FichierService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final FichierRepository fichierRepository;

	@Autowired
	public FichierServiceImpl(FichierRepository fichierRepository) {
		this.fichierRepository = fichierRepository;
	}

	ErrorPattern<Fichier> ep;

	/**
	 * <p>This method check and validate fichier properties before saving fichier</p>
	 * @param fichierJsonRequest contains fichier parameters to check and validate
	 * @see ErrorResponse
	 * @see Fichier
	 * @see FichierJsonRequest
	 * @return the checking and validation response
	 * @since 1.0
	 */

	@Override
	public ErrorResponse <Fichier> beforeSave(FichierJsonRequest fichierJsonRequest){
		List <ErrorMessage> errors = new ArrayList <>();
		ep = new ErrorPattern <>();

		Fichier fichier = new Fichier();

		if (fichierJsonRequest == null){
			logger.warn("Fichier save error : fichier null");
			errors = ep.getError(0, "fichier can not be null", "object fichier can not be null", errors);
		} else {

		}
		return ep.errorResultBuilder(errors, fichier);
	}

	/**
	 * <p>This method persist fichier properties in database</p>
	 * @param fichier contains fichier parameters to check and validate
	 * @return the fichier persist in database
	 * @see Fichier
	 * @since 1.0
	 */

	@Override
	public Fichier save(Fichier fichier) {
		if (fichier == null){
			logger.warn("Fichier save failed");
			throw new ValidationException("fichier can not be null");
		}

		fichier = fichierRepository.save(fichier);
		logger.info("Fichier save successfully", fichier);
		return fichier;
	}

	/**
	 * <p>This method check fichier before deleting</p>
	 * @param fichier to check
	 * @see Fichier
	 * @return the checking response
	 * @see ErrorResponse
	 * @since 1.0
	 */

	@Override
	public ErrorResponse<Fichier> beforeDelete(Fichier fichier){
		List<ErrorMessage> errors = new ArrayList<>();
		ep = new ErrorPattern<>();
		if(fichier == null){
			errors = ep.getError(0, "Fichier not exist", "Fichier not exist", errors);
		}
		return ep.errorResultBuilder(errors, fichier);
	}

	/**
	 * <p>This method delete fichier in database</p>
	 * @param fichier to delete
	 * @see Fichier
	 * @since 1.0
	 */

	@Override
	public void delete(Fichier fichier) {
		if (fichier == null){
			logger.warn("Fichier delete failed");
			throw new ValidationException("fichier can not be null");
		}

		fichierRepository.delete(fichier);
		logger.info("Fichier delete successfully", fichier);
	}

	/**
	 * <p>This method find fichier by id in database</p>
	 * @param id the fichier id in database
	 * @return the fichier found in database
	 * @see Fichier
	 * @since 1.0
	 */

	@Override
	public Fichier findOne(Long id) {
		if (id == null){
			logger.warn("Fichier find by id failed");
			throw new ValidationException("id can not be null");
		}

		Optional<Fichier> fichier = fichierRepository.findById(id);
		if(fichier.isPresent()){
			logger.info("Fichier find by id successfully", fichier);
			return fichier.get();
		} else {
			logger.info("Fichier find by id not exist", fichier);
			return null;
		}
	}

	/**
	 * <p>This method page fichiers in database</p>
	 * @return fichiers page found in database
	 * @see Fichier
	 * @since 1.0
	 */

	@Override
		public Page <Fichier> load(Pageable pageable, String keyword) {
		Page<Fichier> pages;
		if(keyword == null || keyword.equals(""))
			pages = fichierRepository.findAll(pageable);
		else
			pages = null;
		logger.info("All fichiers found successfully", pages.getTotalPages());
		return pages;
	}
}
