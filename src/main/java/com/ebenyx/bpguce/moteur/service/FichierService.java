package com.ebenyx.bpguce.moteur.service;

import com.ebenyx.bpguce.utils.error.ErrorResponse;
import com.ebenyx.bpguce.moteur.entity.Fichier;
import com.ebenyx.bpguce.moteur.json.request.FichierJsonRequest;

import org.springframework.data.domain.Page;;
import org.springframework.data.domain.Pageable;;

public interface FichierService {

	ErrorResponse<Fichier> beforeSave(FichierJsonRequest fichierJsonRequest);

	Fichier save(Fichier fichier);

	ErrorResponse<Fichier> beforeDelete(Fichier fichier);

	void delete(Fichier fichier);

	Fichier findOne(Long id);

	Page<Fichier> load(Pageable pageable, String keyword);

}