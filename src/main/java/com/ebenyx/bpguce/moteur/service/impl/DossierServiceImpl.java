package com.ebenyx.bpguce.moteur.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ebenyx.bpguce.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ebenyx.bpguce.moteur.entity.Dossier;
import com.ebenyx.bpguce.moteur.json.request.DossierJsonRequest;
import com.ebenyx.bpguce.moteur.repository.DossierRepository;
import com.ebenyx.bpguce.moteur.service.DossierService;

import com.ebenyx.bpguce.utils.error.ErrorMessage;

import com.ebenyx.bpguce.utils.error.ErrorPattern;

import com.ebenyx.bpguce.utils.error.ErrorResponse;

/**
 * The DossierService class ...
 * @see DossierService
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
@Transactional
@Service("dossierService")
public class DossierServiceImpl implements DossierService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final DossierRepository dossierRepository;

	@Autowired
	public DossierServiceImpl(DossierRepository dossierRepository) {
		this.dossierRepository = dossierRepository;
	}

	ErrorPattern<Dossier> ep;

	/**
	 * <p>This method check and validate dossier properties before saving dossier</p>
	 * @param dossierJsonRequest contains dossier parameters to check and validate
	 * @see ErrorResponse
	 * @see Dossier
	 * @see DossierJsonRequest
	 * @return the checking and validation response
	 * @since 1.0
	 */

	@Override
	public ErrorResponse <Dossier> beforeSave(DossierJsonRequest dossierJsonRequest){
		List <ErrorMessage> errors = new ArrayList <>();
		ep = new ErrorPattern <>();

		Dossier dossier = new Dossier();

		if (dossierJsonRequest == null){
			logger.warn("Dossier save error : dossier null");
			errors = ep.getError(0, "dossier can not be null", "object dossier can not be null", errors);
		} else {

		}
		return ep.errorResultBuilder(errors, dossier);
	}

	/**
	 * <p>This method persist dossier properties in database</p>
	 * @param dossier contains dossier parameters to check and validate
	 * @return the dossier persist in database
	 * @see Dossier
	 * @since 1.0
	 */

	@Override
	public Dossier save(Dossier dossier) {
		if (dossier == null){
			logger.warn("Dossier save failed");
			throw new ValidationException("dossier can not be null");
		}

		dossier = dossierRepository.save(dossier);
		logger.info("Dossier save successfully", dossier);
		return dossier;
	}

	/**
	 * <p>This method check dossier before deleting</p>
	 * @param dossier to check
	 * @see Dossier
	 * @return the checking response
	 * @see ErrorResponse
	 * @since 1.0
	 */

	@Override
	public ErrorResponse<Dossier> beforeDelete(Dossier dossier){
		List<ErrorMessage> errors = new ArrayList<>();
		ep = new ErrorPattern<>();
		if(dossier == null){
			errors = ep.getError(0, "Dossier not exist", "Dossier not exist", errors);
		}
		return ep.errorResultBuilder(errors, dossier);
	}

	/**
	 * <p>This method delete dossier in database</p>
	 * @param dossier to delete
	 * @see Dossier
	 * @since 1.0
	 */

	@Override
	public void delete(Dossier dossier) {
		if (dossier == null){
			logger.warn("Dossier delete failed");
			throw new ValidationException("dossier can not be null");
		}

		dossierRepository.delete(dossier);
		logger.info("Dossier delete successfully", dossier);
	}

	/**
	 * <p>This method find dossier by id in database</p>
	 * @param id the dossier id in database
	 * @return the dossier found in database
	 * @see Dossier
	 * @since 1.0
	 */

	@Override
	public Dossier findOne(Long id) {
		if (id == null){
			logger.warn("Dossier find by id failed");
			throw new ValidationException("id can not be null");
		}

		Optional<Dossier> dossier = dossierRepository.findById(id);
		if(dossier.isPresent()){
			logger.info("Dossier find by id successfully", dossier);
			return dossier.get();
		} else {
			logger.info("Dossier find by id not exist", dossier);
			return null;
		}
	}

	/**
	 * <p>This method page dossiers in database</p>
	 * @return dossiers page found in database
	 * @see Dossier
	 * @since 1.0
	 */

	@Override
		public Page <Dossier> load(Pageable pageable, String keyword) {
		Page<Dossier> pages;
		if(keyword == null || keyword.equals(""))
			pages = dossierRepository.findAll(pageable);
		else
			pages = null;
		logger.info("All dossiers found successfully", pages.getTotalPages());
		return pages;
	}
}
