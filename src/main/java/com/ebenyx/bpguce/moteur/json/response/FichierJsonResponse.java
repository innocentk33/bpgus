package com.ebenyx.bpguce.moteur.json.response;

import lombok.Getter;
import java.time.LocalDateTime;

import java.io.Serializable;

import com.ebenyx.bpguce.moteur.entity.Fichier;
import com.ebenyx.bpguce.utils.DateUtils;

/**
 * Fichier response body class ...
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
public class FichierJsonResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	private final Long id;

	@Getter
	private final LocalDateTime createDate;

	@Getter
	private final String formatCreateDate;

	@Getter
	private final LocalDateTime lastEditDate;

	@Getter
	private final String formatLastEditDate;

	public FichierJsonResponse(Fichier fichier) {
		this.id = fichier.getId();
		this.createDate = fichier.getCreateDate();
		this.formatCreateDate = DateUtils.dateToString(fichier.getCreateDate());
		this.lastEditDate = fichier.getUpdateDate();
		this.formatLastEditDate = DateUtils.dateToString(fichier.getUpdateDate());
	}

}