package com.ebenyx.bpguce.moteur.json.response;

import lombok.Getter;
import java.time.LocalDateTime;

import java.io.Serializable;

import com.ebenyx.bpguce.moteur.entity.Domiciliation;
import com.ebenyx.bpguce.utils.DateUtils;

/**
 * Domiciliation response body class ...
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
public class DomiciliationJsonResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	private final Long id;

	@Getter
	private final LocalDateTime createDate;

	@Getter
	private final String formatCreateDate;

	@Getter
	private final LocalDateTime lastEditDate;

	@Getter
	private final String formatLastEditDate;

	public DomiciliationJsonResponse(Domiciliation domiciliation) {
		this.id = domiciliation.getId();
		this.createDate = domiciliation.getCreateDate();
		this.formatCreateDate = DateUtils.dateToString(domiciliation.getCreateDate());
		this.lastEditDate = domiciliation.getUpdateDate();
		this.formatLastEditDate = DateUtils.dateToString(domiciliation.getUpdateDate());
	}

}