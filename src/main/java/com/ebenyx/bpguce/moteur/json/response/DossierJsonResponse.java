package com.ebenyx.bpguce.moteur.json.response;

import lombok.Getter;
import java.time.LocalDateTime;

import java.io.Serializable;

import com.ebenyx.bpguce.moteur.entity.Dossier;
import com.ebenyx.bpguce.utils.DateUtils;

/**
 * Dossier response body class ...
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
public class DossierJsonResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	private final Long id;

	@Getter
	private final LocalDateTime createDate;

	@Getter
	private final String formatCreateDate;

	@Getter
	private final LocalDateTime lastEditDate;

	@Getter
	private final String formatLastEditDate;

	public DossierJsonResponse(Dossier dossier) {
		this.id = dossier.getId();
		this.createDate = dossier.getCreateDate();
		this.formatCreateDate = DateUtils.dateToString(dossier.getCreateDate());
		this.lastEditDate = dossier.getUpdateDate();
		this.formatLastEditDate = DateUtils.dateToString(dossier.getUpdateDate());
	}

}