package com.ebenyx.bpguce.moteur.json.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Fichier request body class ...
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
@NoArgsConstructor
@AllArgsConstructor
public class FichierJsonRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	private Long id;

}