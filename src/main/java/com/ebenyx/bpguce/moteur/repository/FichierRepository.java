package com.ebenyx.bpguce.moteur.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ebenyx.bpguce.moteur.entity.Fichier;

public interface FichierRepository extends JpaRepository<Fichier, Long> {

	List<Fichier> findAll();

	Page<Fichier> findAll(Pageable pageable);

}