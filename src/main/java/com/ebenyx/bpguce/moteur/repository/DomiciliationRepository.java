package com.ebenyx.bpguce.moteur.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ebenyx.bpguce.moteur.entity.Domiciliation;

public interface DomiciliationRepository extends JpaRepository<Domiciliation, Long> {

	List<Domiciliation> findAll();

	Page<Domiciliation> findAll(Pageable pageable);

}