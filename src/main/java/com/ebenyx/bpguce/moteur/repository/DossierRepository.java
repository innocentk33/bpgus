package com.ebenyx.bpguce.moteur.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ebenyx.bpguce.moteur.entity.Dossier;

public interface DossierRepository extends JpaRepository<Dossier, Long> {

	List<Dossier> findAll();

	Page<Dossier> findAll(Pageable pageable);

}