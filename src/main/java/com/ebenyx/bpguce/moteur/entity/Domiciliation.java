package com.ebenyx.bpguce.moteur.entity;

import javax.persistence.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.ebenyx.bpguce.utils.BaseEntity;

/**
 * Domiciliation entity class ...
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Scope("prototype")
@Component("myDomiciliation")
@Table(name="Domiciliation")
public class Domiciliation extends BaseEntity<Long>{

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="refDomiciliation")
	private String refdomiciliation;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="nomemail")
	private String nomemail;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="email")
	private String email;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="address")
	private String address;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="telephone")
	private String telephone;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="typeClient")
	private String typeclient;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="numCompteContribuable")
	private String numcomptecontribuable;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="numCompte")
	private String numcompte;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="rib")
	private String rib;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="dateValidation")
	private LocalDate datevalidation;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="enabled")
	private Boolean enabled;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="status")
	private String status;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="codAgence")
	private String codagence;

	/**
	 * <p> Domiciliation ...</p>
	 */

	@Getter @Setter
	@Column(name="frais")
	private Long frais;

}