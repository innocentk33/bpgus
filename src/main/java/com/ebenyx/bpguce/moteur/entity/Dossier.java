package com.ebenyx.bpguce.moteur.entity;

import javax.persistence.*;
import org.springframework.context.annotation.Scope;
import java.util.List;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.ebenyx.bpguce.utils.BaseEntity;

/**
 * Dossier entity class ...
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Scope("prototype")
@Component("myDossier")
@Table(name="Dossier")
public class Dossier extends BaseEntity<Long>{

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="declarant")
	private String declarant;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="adressDeclarant")
	private String adressdeclarant;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="paysExportation")
	private String paysexportation;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="paysDestination")
	private String paysdestination;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="modeTransport")
	private String modetransport;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="typeChargement")
	private String typechargement;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="lieuChargement")
	private String lieuchargement;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="masseNet")
	private Long massenet;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="masseBrute")
	private Long massebrute;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="refDomiciliation")
	private String refdomiciliation;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="dateDomciliation")
	private LocalDate datedomciliation;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="codBanque")
	private String codbanque;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="codAgence")
	private String codagence;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="refFacture")
	private String reffacture;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="dateFacture")
	private LocalDate datefacture;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="deviseFacture")
	private String devisefacture;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="typeFacture")
	private String typefacture;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="refDomiciliation")
	private String refdomiciliation;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="valeurTotaleFOB")
	private Double valeurtotalefob;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="valeurTotaleFret")
	private Double valeurtotalefret;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="valeurTotaleAssurance")
	private Double valeurtotaleassurance;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="valeurTotalCAF")
	private Double valeurtotalcaf;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="incotem")
	private String incotem;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@Column(name="modePaiement")
	private String modepaiement;

	/**
	 * <p> Dossier ...</p>
	 */

	@Getter @Setter
	@OneToMany(mappedBy = "dossier")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List <Fichier> fichier;

}