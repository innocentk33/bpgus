package com.ebenyx.bpguce.moteur.entity;

import javax.persistence.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.ebenyx.bpguce.utils.BaseEntity;

/**
 * Fichier entity class ...
 * @author Kacou innocent
 * @version 1.0, 16/12/2019
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Scope("prototype")
@Component("myFichier")
@Table(name="Fichier")
public class Fichier extends BaseEntity<Long>{

	/**
	 * <p> Fichier ...</p>
	 */

	@Getter @Setter
	@Column(name="url")
	private String url;

	/**
	 * <p> Fichier ...</p>
	 */

	@Getter @Setter
	@Column(name="type")
	private String type;

	/**
	 * <p> Fichier ...</p>
	 */

	@Getter @Setter
	@Column(name="uploadDate")
	private LocalDate uploaddate;

	/**
	 * <p> Fichier ...</p>
	 */

	@Getter @Setter
	@Column(name="enabled")
	private Boolean enabled;

	/**
	 * <p> Fichier ...</p>
	 */

	@Getter @Setter
	@Column(name="nom")
	private String nom;

	/**
	 * <p> Fichier ...</p>
	 */

	@Getter @Setter
	@Column(name="refDoc")
	private String refdoc;

	/**
	 * <p> Fichier ...</p>
	 */

	@ManyToOne
	@Getter @Setter
	@JoinColumn(name = "Dossier", nullable=false)
	private Dossier dossier;

}