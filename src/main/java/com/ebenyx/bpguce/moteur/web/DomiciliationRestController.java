package com.ebenyx.bpguce.moteur.web;

import com.ebenyx.bpguce.security.JwtTokenUtil;
import com.ebenyx.bpguce.moteur.entity.Domiciliation;
import com.ebenyx.bpguce.moteur.json.request.DomiciliationJsonRequest;
import com.ebenyx.bpguce.moteur.json.response.DomiciliationJsonResponse;
import com.ebenyx.bpguce.moteur.service.DomiciliationService;

import com.ebenyx.bpguce.utils.Constants;
import com.ebenyx.bpguce.utils.RestApiResponse;
import com.ebenyx.bpguce.utils.error.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/settings/domiciliation")
public class DomiciliationRestController {

	private final DomiciliationService domiciliationService;

	private final JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	public DomiciliationRestController(DomiciliationService domiciliationService, JwtTokenUtil jwtTokenUtil) {
		this.domiciliationService = domiciliationService;
		this.jwtTokenUtil = jwtTokenUtil;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody DomiciliationJsonRequest domiciliationJsonRequest, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			RestApiResponse <DomiciliationJsonResponse> restApiResponse = new RestApiResponse<>();
			ErrorResponse <Domiciliation> errorResponse = domiciliationService.beforeSave(domiciliationJsonRequest);
			if(errorResponse.getError())
				return ResponseEntity.ok(errorResponse);
			else {
				Domiciliation domiciliation = errorResponse.getEntity();
				restApiResponse.setMessage((domiciliation.getId() == null) ? "? enregistrée avec succès" : "? modifiée avec succès");
				domiciliation = domiciliationService.save(domiciliation);
				DomiciliationJsonResponse  domiciliationJsonResponse = new DomiciliationJsonResponse(domiciliation);
				restApiResponse.setRow(domiciliationJsonResponse);
				restApiResponse.setError(false);
				return ResponseEntity.ok(restApiResponse);
			}
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestParam(value="id") Long id, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			Domiciliation domiciliation = domiciliationService.findOne(id);
			ErrorResponse <Domiciliation> errorResponse = domiciliationService.beforeDelete(domiciliation);
			if(errorResponse.getError())
				return ResponseEntity.ok(errorResponse);
			else {
				RestApiResponse<DomiciliationJsonResponse> restApiResponse = new RestApiResponse<>();
				domiciliationService.delete(domiciliation);
				restApiResponse.setError(false);
				restApiResponse.setMessage("? supprimé avec succès");
				return ResponseEntity.ok(restApiResponse);
			}
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/find-one", method = RequestMethod.GET)
	public ResponseEntity<?> findOne(@RequestParam(value="id") Long id, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			RestApiResponse<DomiciliationJsonResponse> restApiResponse = new RestApiResponse<>();
			Domiciliation domiciliation = domiciliationService.findOne(id);
			restApiResponse.setError(false);
			if(domiciliation == null){
				restApiResponse.setMessage("? inexistant");
			} else {
				DomiciliationJsonResponse domiciliationJsonResponse = new DomiciliationJsonResponse(domiciliation);
				restApiResponse.setRow(domiciliationJsonResponse);
			}
				return ResponseEntity.ok(restApiResponse);
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/load", method = RequestMethod.GET)
	public ResponseEntity<?> load(@RequestParam(defaultValue = "0") int page, @RequestParam(value="keyword", required=false) String keyword, HttpServletRequest request) {
		RestApiResponse<DomiciliationJsonResponse> restApiResponse = new RestApiResponse<>();
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			Page <Domiciliation> pages = domiciliationService.load(PageRequest.of(page, Constants.DEFAULT_PAGE_SIZE, Constants.DEFAULT_SORT_DIRECTION, "createDate"), keyword);
			List <DomiciliationJsonResponse> list = new ArrayList <>();
			for(Domiciliation domiciliation : pages.getContent()){
				list.add(new DomiciliationJsonResponse(domiciliation));
			}
			restApiResponse.setRows(list);
			restApiResponse.setMessage((pages.getTotalPages() == 0) ? "Aucun ? trouvé" : String.format("Total %d/%d", pages.getNumberOfElements(), pages.getTotalElements()));
			return ResponseEntity.ok(restApiResponse);
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}
}
