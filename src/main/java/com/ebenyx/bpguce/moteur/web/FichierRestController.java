package com.ebenyx.bpguce.moteur.web;

import com.ebenyx.bpguce.security.JwtTokenUtil;
import com.ebenyx.bpguce.moteur.entity.Fichier;
import com.ebenyx.bpguce.moteur.json.request.FichierJsonRequest;
import com.ebenyx.bpguce.moteur.json.response.FichierJsonResponse;
import com.ebenyx.bpguce.moteur.service.FichierService;

import com.ebenyx.bpguce.utils.Constants;
import com.ebenyx.bpguce.utils.RestApiResponse;
import com.ebenyx.bpguce.utils.error.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/settings/fichier")
public class FichierRestController {

	private final FichierService fichierService;

	private final JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	public FichierRestController(FichierService fichierService, JwtTokenUtil jwtTokenUtil) {
		this.fichierService = fichierService;
		this.jwtTokenUtil = jwtTokenUtil;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody FichierJsonRequest fichierJsonRequest, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			RestApiResponse <FichierJsonResponse> restApiResponse = new RestApiResponse<>();
			ErrorResponse <Fichier> errorResponse = fichierService.beforeSave(fichierJsonRequest);
			if(errorResponse.getError())
				return ResponseEntity.ok(errorResponse);
			else {
				Fichier fichier = errorResponse.getEntity();
				restApiResponse.setMessage((fichier.getId() == null) ? "? enregistrée avec succès" : "? modifiée avec succès");
				fichier = fichierService.save(fichier);
				FichierJsonResponse  fichierJsonResponse = new FichierJsonResponse(fichier);
				restApiResponse.setRow(fichierJsonResponse);
				restApiResponse.setError(false);
				return ResponseEntity.ok(restApiResponse);
			}
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestParam(value="id") Long id, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			Fichier fichier = fichierService.findOne(id);
			ErrorResponse <Fichier> errorResponse = fichierService.beforeDelete(fichier);
			if(errorResponse.getError())
				return ResponseEntity.ok(errorResponse);
			else {
				RestApiResponse<FichierJsonResponse> restApiResponse = new RestApiResponse<>();
				fichierService.delete(fichier);
				restApiResponse.setError(false);
				restApiResponse.setMessage("? supprimé avec succès");
				return ResponseEntity.ok(restApiResponse);
			}
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/find-one", method = RequestMethod.GET)
	public ResponseEntity<?> findOne(@RequestParam(value="id") Long id, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			RestApiResponse<FichierJsonResponse> restApiResponse = new RestApiResponse<>();
			Fichier fichier = fichierService.findOne(id);
			restApiResponse.setError(false);
			if(fichier == null){
				restApiResponse.setMessage("? inexistant");
			} else {
				FichierJsonResponse fichierJsonResponse = new FichierJsonResponse(fichier);
				restApiResponse.setRow(fichierJsonResponse);
			}
				return ResponseEntity.ok(restApiResponse);
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/load", method = RequestMethod.GET)
	public ResponseEntity<?> load(@RequestParam(defaultValue = "0") int page, @RequestParam(value="keyword", required=false) String keyword, HttpServletRequest request) {
		RestApiResponse<FichierJsonResponse> restApiResponse = new RestApiResponse<>();
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			Page <Fichier> pages = fichierService.load(PageRequest.of(page, Constants.DEFAULT_PAGE_SIZE, Constants.DEFAULT_SORT_DIRECTION, "createDate"), keyword);
			List <FichierJsonResponse> list = new ArrayList <>();
			for(Fichier fichier : pages.getContent()){
				list.add(new FichierJsonResponse(fichier));
			}
			restApiResponse.setRows(list);
			restApiResponse.setMessage((pages.getTotalPages() == 0) ? "Aucun ? trouvé" : String.format("Total %d/%d", pages.getNumberOfElements(), pages.getTotalElements()));
			return ResponseEntity.ok(restApiResponse);
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}
}
