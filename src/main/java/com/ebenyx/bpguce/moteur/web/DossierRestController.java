package com.ebenyx.bpguce.moteur.web;

import com.ebenyx.bpguce.security.JwtTokenUtil;
import com.ebenyx.bpguce.moteur.entity.Dossier;
import com.ebenyx.bpguce.moteur.json.request.DossierJsonRequest;
import com.ebenyx.bpguce.moteur.json.response.DossierJsonResponse;
import com.ebenyx.bpguce.moteur.service.DossierService;

import com.ebenyx.bpguce.utils.Constants;
import com.ebenyx.bpguce.utils.RestApiResponse;
import com.ebenyx.bpguce.utils.error.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/settings/dossier")
public class DossierRestController {

	private final DossierService dossierService;

	private final JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	public DossierRestController(DossierService dossierService, JwtTokenUtil jwtTokenUtil) {
		this.dossierService = dossierService;
		this.jwtTokenUtil = jwtTokenUtil;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<?> add(@RequestBody DossierJsonRequest dossierJsonRequest, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			RestApiResponse <DossierJsonResponse> restApiResponse = new RestApiResponse<>();
			ErrorResponse <Dossier> errorResponse = dossierService.beforeSave(dossierJsonRequest);
			if(errorResponse.getError())
				return ResponseEntity.ok(errorResponse);
			else {
				Dossier dossier = errorResponse.getEntity();
				restApiResponse.setMessage((dossier.getId() == null) ? "? enregistrée avec succès" : "? modifiée avec succès");
				dossier = dossierService.save(dossier);
				DossierJsonResponse  dossierJsonResponse = new DossierJsonResponse(dossier);
				restApiResponse.setRow(dossierJsonResponse);
				restApiResponse.setError(false);
				return ResponseEntity.ok(restApiResponse);
			}
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@RequestParam(value="id") Long id, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			Dossier dossier = dossierService.findOne(id);
			ErrorResponse <Dossier> errorResponse = dossierService.beforeDelete(dossier);
			if(errorResponse.getError())
				return ResponseEntity.ok(errorResponse);
			else {
				RestApiResponse<DossierJsonResponse> restApiResponse = new RestApiResponse<>();
				dossierService.delete(dossier);
				restApiResponse.setError(false);
				restApiResponse.setMessage("? supprimé avec succès");
				return ResponseEntity.ok(restApiResponse);
			}
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/find-one", method = RequestMethod.GET)
	public ResponseEntity<?> findOne(@RequestParam(value="id") Long id, HttpServletRequest request) {
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			RestApiResponse<DossierJsonResponse> restApiResponse = new RestApiResponse<>();
			Dossier dossier = dossierService.findOne(id);
			restApiResponse.setError(false);
			if(dossier == null){
				restApiResponse.setMessage("? inexistant");
			} else {
				DossierJsonResponse dossierJsonResponse = new DossierJsonResponse(dossier);
				restApiResponse.setRow(dossierJsonResponse);
			}
				return ResponseEntity.ok(restApiResponse);
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

	@RequestMapping(value = "/load", method = RequestMethod.GET)
	public ResponseEntity<?> load(@RequestParam(defaultValue = "0") int page, @RequestParam(value="keyword", required=false) String keyword, HttpServletRequest request) {
		RestApiResponse<DossierJsonResponse> restApiResponse = new RestApiResponse<>();
		if (jwtTokenUtil.canTokenBeRefreshed(request)) {
			Page <Dossier> pages = dossierService.load(PageRequest.of(page, Constants.DEFAULT_PAGE_SIZE, Constants.DEFAULT_SORT_DIRECTION, "createDate"), keyword);
			List <DossierJsonResponse> list = new ArrayList <>();
			for(Dossier dossier : pages.getContent()){
				list.add(new DossierJsonResponse(dossier));
			}
			restApiResponse.setRows(list);
			restApiResponse.setMessage((pages.getTotalPages() == 0) ? "Aucun ? trouvé" : String.format("Total %d/%d", pages.getNumberOfElements(), pages.getTotalElements()));
			return ResponseEntity.ok(restApiResponse);
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}
}
