package com.ebenyx.bpguce.exception;

/**
 * Authentication eception manager.
 *
 * @author Kacou innocent
 * @version 1.0
 */
public class AuthenticationException extends RuntimeException {
	public AuthenticationException(String message, Throwable cause) {
		super(message, cause);
	}
}
