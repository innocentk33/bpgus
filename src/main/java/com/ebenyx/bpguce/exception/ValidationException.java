package com.ebenyx.bpguce.exception;

import org.springframework.transaction.annotation.Transactional;

/**
 * Data validation exception.
 *
 * @author Kacou innocent
 * @version 1.0
 */

@Transactional(rollbackFor = ValidationException.class)
public class ValidationException extends RuntimeException {

	public ValidationException(String message) {
		super(message);
	}

	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}
}
