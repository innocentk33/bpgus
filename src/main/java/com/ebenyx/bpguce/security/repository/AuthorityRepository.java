package com.ebenyx.bpguce.security.repository;

import com.ebenyx.bpguce.security.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
	List<Authority> findAll();
}
