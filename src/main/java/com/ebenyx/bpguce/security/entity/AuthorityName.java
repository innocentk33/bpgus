package com.ebenyx.bpguce.security.entity;

public enum AuthorityName {
	ROLE_USER, ROLE_ADMIN
}
