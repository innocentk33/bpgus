package com.ebenyx.bpguce.security.service;

import com.ebenyx.bpguce.security.entity.User;

import com.ebenyx.bpguce.utils.error.ErrorResponse;

import java.util.List;

public interface UserService {

	ErrorResponse <User> beforeSave(User user);

	User save(User user);

	ErrorResponse <User> beforeDelete(User user);

	void delete(User user);

	User findOne(Long id);

	User findByUsername(String username);

	List<User> findAll();

	List<User> search(String keyword);
}
